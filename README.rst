HydrOffice QC Tools 2
=====================

.. image:: https://img.shields.io/pypi/v/hyo.qc2.svg
    :target: https://pypi.python.org/pypi/hyo.qc2
    :alt: PyPi version

.. image:: https://img.shields.io/badge/docs-latest-brightgreen.svg
    :target: https://www.hydroffice.org/manuals/qctools2/index.html
    :alt: Latest Documentation

.. image:: https://ci.appveyor.com/api/projects/status/bwlc3h00jyl2upqw?svg=true
    :target: https://ci.appveyor.com/project/appveyor1/hyo-qc2
    :alt: AppVeyor Status

.. image:: https://travis-ci.org/hydroffice/hyo_qc2.svg?branch=master
    :target: https://travis-ci.com/hydroffice/hyo_qc2
    :alt: Travis-CI Status

.. image:: https://api.codacy.com/project/badge/Grade/963b5a103fc8445a98faff92b77b9ed7
    :target: https://www.codacy.com/app/hydroffice/hyo_qc2/dashboard
    :alt: Codacy badge

* Code: `GitHub repo <https://github.com/hydroffice/hyo_qc2>`_
* Project page: `url <https://www.hydroffice.org/qctools/main>`_
* Download page: `url <https://www.hydroffice.org/qctools/main>`_
* License: LGPLv3 license (See `LICENSE <https://github.com/hydroffice/hyo_qc2/raw/master/LICENSE>`_)

|

General info
------------

.. image:: https://github.com/hydroffice/hyo_qc2/raw/master/hyo/qc2/qctools2/media/favicon.png
    :alt: logo

HydrOffice is a research development environment for ocean mapping. Its aim is to provide a collection of
hydro-packages to deal with specific issues in such a field, speeding up both algorithms testing and
research-2-operation.

This package provides functionalities to apply quality controls to survey data.
